# Tidy Tuesday - Week #11
### Board Games


See https://github.com/rfordatascience/tidytuesday for more information.

![](top5games.png)

## Code Snippet
```r
library(tidyverse)
library(ghibli)

# load data
board_games <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-03-12/board_games.csv")

# process data (slightly)
processed_bgs <- board_games %>%
  mutate(main_category = str_remove(category, ",.*$"),   # Assumed the first category is the main category
         decade = 10 * year_published %/% 10)            # Create decades

# create top 5 chart, by decade
processed_bgs %>%
  group_by(decade) %>%
  select(decade, name, main_category, average_rating) %>%
  top_n(5) %>% 
  mutate(index = rank(desc(average_rating)),
         index = reorder(index, average_rating)) %>% 
  ggplot(aes(index, average_rating)) +
  geom_col(aes(fill = as.factor(decade)), show.legend = F, position = 'dodge') +
  scale_fill_manual(values = ghibli_palettes$MononokeDark) +
  coord_flip() +
  geom_text(aes(label = paste0(name, " (",main_category,")"), x = index), 
            y = 0.1, hjust = 0, vjust = 0.5, family = my_font, col = my_bkgd, size = rel(3)) +
  facet_wrap(~ decade, ncol = 1, strip.position = "left") +
  labs(x = NULL, y = "Average Rating",
       title = "Top 5 Games By Average Rating in each Decade",
       subtitle = "Name (Main Category)", 
       caption = "Source: Board Game Geek  |  By @DaveBloom11") +
  theme(axis.text.y = element_blank(),
        strip.background = element_rect(fill = ghibli_palettes$MononokeMedium[1]),
        strip.text = element_text(size = rel(1.1)),
        plot.title = element_text(hjust = -0.1),
        plot.subtitle = element_text(hjust = -0.04))

```
